from django.shortcuts import render
from django.http import HttpResponse
from .models import Persona, Perrito 
from django.shortcuts import redirect
# Create your views here.

#importar user
from django.contrib.auth.models import User, Group
#sistema de autenticación 
from django.contrib.auth import authenticate,logout, login as auth_login

from django.contrib.auth.decorators import login_required

def registro(request):
    return render(request,'registro.html',{})

def index(request):
    return render(request,'index.html',{'Registro de personas':Persona.objects.all()})

def registroPerro(request):
    return render(request, 'registroPerro.html', {'Personas': Persona.objects.all(), 'Perritos': Perrito.objects.all()})

def crearPerro(request):
    nombrePerro = request.POST.get('nombrePerro','')
    raza = request.POST.get('raza','')
    imagen = request.FILES.get('imagen', False)
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')
    perrito = Perrito(nombrePerro = nombrePerro, raza=raza, imagen=imagen, descripcion=descripcion, estado=estado)
    perrito.save()
    return redirect('index')    

def crear(request):
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    username = request.POST.get('username','')
    correo = request.POST.get('correo','')
    password = request.POST.get('password','')
    telefono = request.POST.get('telefono','')
    rut = request.POST.get('rut','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    vivienda = request.POST.get('vivienda','')
    persona = Persona(nombre=nombre,apellido=apellido, username=username,correo=correo,password=password,telefono=telefono,rut=rut,region=region,comuna=comuna,vivienda=vivienda)
    persona.save()

    persona = User.objects.create_user(username, correo, password)
    persona.save()
    
    return redirect('index')

def cerrar_sesion(request):
    logout(request)
    return redirect('index')

def login(request):
    return render(request,'login.html',{'personas': Persona.objects.all()})

def login_iniciar(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    print(username,password)
    user = authenticate(request, username=username, password=password)

    if user is not None:
        auth_login(request, user)
        return redirect("index")
    else:
        return redirect("login")


    