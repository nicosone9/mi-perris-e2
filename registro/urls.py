from django.urls import path
from . import views


urlpatterns = [
    
    path('',views.index,name="index"),
    path('registro/',views.registro, name="registro"),
    path('login/',views.login, name="login"),
    path('registro/crear',views.crear,name="crear"),
    path('registroPerro/',views.registroPerro,name="registroPerro"),
    path('registroPerro/crearPerro',views.crearPerro,name="crearPerro"),
    path('login/cerrar_sesion/',views.cerrar_sesion,name="cerrar_sesion"),
    path('login/login_iniciar',views.login_iniciar,name="login_iniciar")

]
