#from _future_ import unicode_literals
from django.db import models

# Create your models here.

class Persona(models.Model):
    nombre = models.CharField(max_length=40)
    apellido = models.CharField(max_length=40)
    username = models.CharField(max_length=50, unique =True)
    correo = models.EmailField(max_length=40)
    password = models.CharField(max_length=50)
    telefono = models.IntegerField()
    rut = models.CharField(max_length=15)
    region = models.CharField(max_length=60)
    comuna = models.CharField(max_length=60)
    vivienda = models.CharField(max_length=60)

    def __str__(self):
        return "persona" 

    
class Perrito(models.Model):
    nombrePerro = models.CharField(max_length=30)
    raza = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=100)
    imagen = models.ImageField(upload_to= 'perro/')
    estado = models.CharField(max_length=50)

    def __str__(self):
        return "perrito" 


