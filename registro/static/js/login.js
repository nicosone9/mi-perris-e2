$("#login").validate({
    rules: {
        nombre_usuario: {
            required: true,
            email: true
            
        },
        contrasenia: {
            required:true
        }
    },
    messages: {
        nombre_usuario: {
            required: 'Este campo es obligatorio',
            email: 'Ingrese una dirección de correo valida'
        },
        contrasenia: {
            required: 'Este campo es obligatorio'
        }
    }
})